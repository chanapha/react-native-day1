
import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image } from "react-native";

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.container}>
          <View style={styles.content}>

          <View style={styles.row}>

            <View style={styles.box1}>
              <Text style={styles}>/</Text>
              <View style={styles.box3}>
              <Text style={styles}>Lorem</Text>
              <View style={styles.box3}>
              <Text style={styles}>Lorem</Text>
              <View style={styles.box3}>
              <Text style={styles}>Lorem</Text>
           
            </View>
           
            </View>         
            </View>      
            </View>

            <View style={styles.box2}>
              <Text style={styles}>Lorem</Text>
            </View>

            <View style={styles.box1}>
              <Text style={styles}>Lorem</Text>
            </View>

            </View>

   
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  header: {
    backgroundColor: "red",
    alignItems: "center"
  },
  headerText: {
    color: "white",
    fontWeight: "bold",
    padding: 30
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "white"
  },
  content: {
    backgroundColor: "yellow",
    flex: 1,
    flexDirection: "row"
  },
  box1: {
    backgroundColor: "white",
    width: 450, 
    height: 50,
  },
  box2: {
    backgroundColor: "purple",
    flex: 1,
    margin: 14
  },
  box3: {
    backgroundColor: "green",
    width: 50, 
    height: 50,
  },
  row: {
    backgroundColor: "cyan",
    flex: 1,
    flexDirection: "row"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
