import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image } from "react-native";

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.row}>

            <View style={styles.box1}>
              <Text style={styles}>/</Text>
              <View style={styles.box6}>
              <Text style={styles.headerText}>Image</Text>
              </View>

            </View>

            <View style={styles.box2}>
              <Text style={styles}>//</Text>

              <View style={styles.box3}>
                <Text style={styles.headerText}>TextInput</Text>
              </View>

              <View style={styles.box3}>
                <Text style={styles.headerText}>TextInput</Text>
              </View>

              <View style={styles.box4}>
                <Text style={styles.headerText2}>TouchableOpacity</Text>
              </View>

            </View>

          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  header: {
    backgroundColor: "red",
    alignItems: "center"
  },
  headerText: {
    color: "black",
    fontWeight: "bold",
    fontSize: 30,
    textAlign: 'center'
  },
  headerText2: {
    color: "white",
    fontSize: 30,
    textAlign: 'center'
  },

  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: "white"
  },
  content: {
    backgroundColor: "yellow",
    flex: 1,
    flexDirection: "row"
  },
  box1: {
    backgroundColor: "#808080",
    flex: 1
  },
  box2: {
    backgroundColor: "#808080",
    flex: 1
  },
  box3: {
    backgroundColor: "#DCDCDC",
    width: 350, 
    height: 50,
    margin: 30

  },
  box4: {
    backgroundColor: "#000000",
    width: 350, 
    height: 50,
    margin: 30
  },
  box5: {
    backgroundColor: "pink",
    width: 350, 
    height: 200,
    margin: 30
  },
  box6: {
    backgroundColor: "white",
    borderRadius: 100,
    width: 200, 
    height: 200,
    margin: 30,
    alignItems: 'center',

  },


  row: {
    backgroundColor: "cyan",
    flex: 1,
    flexDirection: "column"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
